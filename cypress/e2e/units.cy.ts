import * as check from "../actions/check";
import * as table from "../units/table";
import { textAreaByLabel, headerButton, messageDialog, siderList, actionsButton, inputByLabel, panel, editor } from "../units/components";
import { goToUnits } from "../actions/goTo";
import { login } from "../actions/login";
import { faker } from '@faker-js/faker';

describe('Units', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
  });

  it('Create unit', () => {
    goToUnits();
    // Prepare data for filling
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const description = faker.lorem.sentence();
    const valueName = faker.string.alpha(5);
    const valueDName = faker.string.alpha({ length: 5, casing: 'upper' });
    // Click Add unit
    actionsButton(1).click();
    // Fill unit fields
    inputByLabel('Name').type(name).should('have.value', name);
    inputByLabel('Display name').type(displayName).should('have.value', displayName);
    textAreaByLabel('Description').type(description).should('have.value', description);
    // Fill unit content
    panel('Unit content').within(() => {
      table.cell(1, 2).contains('unset').click();
      table.cell(1, 2).find('input').type(valueName + '{enter}');
      table.cell(1, 3).contains('unset').click();
      table.cell(1, 3).find('input').type(valueDName + '{enter}');
    });
    // Click Save
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Unit saved');
    // Check units appears in list
    siderList().should('contains.text', displayName);
    // Check fields
    inputByLabel('Name').should('have.value', name);
    inputByLabel('Display name').should('have.value', displayName);
    textAreaByLabel('Description').should('have.value', description);
    // Check unit content
    panel('Unit content').within(() => {
      table.cell(1, 2).should('have.text', valueName);
      table.cell(1, 3).should('have.text', valueDName);
      table.cell(1, 4).should('have.text', 'value');
      table.cell(1, 5).find('input.ant-checkbox-input').should('be.checked');
    });
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Edit unit', () => {
    // Prepare data
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const updDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const valueName = faker.string.alpha(5);
    const valueDName = faker.string.alpha({ length: 5, casing: 'upper' });
    cy.createUnit(name, displayName, valueName, valueDName);
    goToUnits();
    // Open created unit
    siderList().contains(displayName).click();
    // Edit unit display name
    inputByLabel('Display name').click().clear().type(updDName).should('have.value', updDName);
    // Update unit content
    panel('Unit content').within(() => {
      // Edit first unit
      table.rowByText(valueName + '1').invoke('index').then((i) => {
        table.cell(i + 1, 2).contains(valueName + '1').click();
        table.cell(i + 1, 2).find('input').clear().type(valueName + 'upd{enter}');
        table.cell(i + 1, 3).contains(valueDName + '1').click();
        table.cell(i + 1, 3).find('input').clear().type(valueDName + 'UPD{enter}');
        table.cell(i + 1, 4).contains('value').click();
        table.cell(i + 1, 4).find('input').clear().type('value/2');
      });
      // Delete second unit
      table.rowByText(valueName + '2').trigger('mouseover').find('label.ant-checkbox-wrapper').eq(0).click();
      cy.contains('button', 'Remove').should('be.visible').click();
      // Add new unit
      cy.contains('button', 'Add measurement unit').click();
      table.cell(2, 2).contains('unset').click();
      table.cell(2, 2).find('input').type(valueName + 'add{enter}');
      table.cell(2, 3).contains('unset').click();
      table.cell(2, 3).find('input').type(valueDName + 'ADD{enter}');
      table.cell(2, 5).find('input.ant-checkbox-input').click();
    });
    // Click Save
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Unit saved');
    // Check updated unit appears in list
    siderList().should('contains.text', updDName);
    siderList().should('not.contains.text', displayName);
    // Check values
    inputByLabel('Display name').should('have.value', updDName);
    panel('Unit content').within(() => {
      table.rowByText(valueName + '2').should('not.exist');
      table.cell(1, 2).should('have.text', valueName + 'upd');
      table.cell(1, 3).should('have.text', valueDName + 'UPD');
      table.cell(1, 4).should('have.text', 'value/2');
      table.cell(1, 5).find('input.ant-checkbox-input').should('not.be.checked');
      table.cell(2, 2).should('have.text', valueName + 'add');
      table.cell(2, 3).should('have.text', valueDName + 'ADD');
      table.cell(2, 4).should('have.text', 'value');
      table.cell(2, 5).find('input.ant-checkbox-input').should('be.checked');
    });
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Delete unit', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    cy.createUnit(faker.string.alpha(10), displayName, faker.string.alpha(5), faker.string.alpha({ length: 5, casing: 'upper' }));
    goToUnits();
    // Open created unit
    siderList().contains(displayName).click();
    // Click Delete
    headerButton(2).click();
    // Confirmation box should appear
    messageDialog().should('be.visible');
    // Click Continue button
    messageDialog().find('button[data-qaid=confirm]').click();
    // Check popup notification message
    check.toast('Unit deleted');
    // Check units not appears in list
    siderList().should('not.contains.text', displayName);
    // Check editor is closed
    editor('Unit').should('not.exist');
  });

  it('Unit: Required fields', () => {
    goToUnits();
    // Click Add unit
    actionsButton(1).click();
    // Fill not required field
    textAreaByLabel('Description').type('something');
    // Click Save
    cy.contains('button', 'Save').click();
    check.toast('Validation error');
    // Check for an error in tooltip
    check.fieldErrorText('Name', 'Name is required');
    check.fieldErrorText('Display name', 'Display name is required');
    // Check for errors in content panel
    cy.get('div.ud-inline-message-type-error').should('be.visible').
      and('contains.text', 'Unit has error').
      and('contains.text', 'Name').
      and('contains.text', 'Display name');
    cy.get('ul[class^=errorList]').should('have.length', 2).each(($el) => {
      cy.wrap($el).should('contains.text', 'The field must be filled in on each row');
    });
  });

  it('Unit: Incorrect values', () => {
    goToUnits();
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const valueDName = faker.string.alpha({ length: 5, casing: 'upper' });
    // Click Add unit
    actionsButton(1).click();
    // Fill unit fields
    inputByLabel('Name').type('name!');
    inputByLabel('Display name').type(displayName);
    // Fill unit content
    panel('Unit content').within(() => {
      table.cell(1, 2).contains('unset').click();
      table.cell(1, 2).find('input').type('name!{enter}');
      table.cell(1, 3).contains('unset').click();
      table.cell(1, 3).find('input').type(valueDName + '{enter}');
      table.cell(1, 4).contains('value').click();
      table.cell(1, 4).find('input').clear().type('!value!');
    });
    // Click Save
    cy.contains('button', 'Save').click();
    check.toast('Validation error');
    // Check for an error in tooltip
    check.fieldErrorText('Name', 'The value must contain latin letters, numbers, symbols "-", "_" and start with a letter. Cannot contain spaces.');
    // Check for errors in content panel
    cy.get('ul[class^=errorList]').eq(1).should('contains.text', 'The value must contain latin letters, numbers, symbols "-", "_" and start with a letter. Cannot contain spaces.');
    cy.get('ul[class^=errorList]').eq(0).should('contains.text', 'The field must contain JavaScript code that returns float number').and('contains.text', 'Basic unit should have conversion formula set as "value"');
  });

  it('Unit: Save button state', () => {
    goToUnits();
    // Click Add button
    actionsButton(1).click();
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
    // Fill some property
    inputByLabel('Name').type('smth');
    // Check Save button is enabled
    check.buttonIsEnabled(cy.contains('button', 'Save'));
    // Clear the value
    inputByLabel('Name').clear();
    // Check Save button is disabled again
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

});
