import * as check from "../actions/check";
import * as create from "../actions/create";
import { fieldByLabel, headerButton, siderList, messageDialog, inputByLabel, textAreaByLabel, actionsButton, editor, optionList } from "../units/components";
import { goToJobs } from "../actions/goTo";
import { login } from "../actions/login";
import { faker } from '@faker-js/faker';

describe('Jobs', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
  });

  it('Create job', () => {
    goToJobs();
    cy.waitForNetworkIdle(1000);
    // Click Add Job button
    cy.get('button[data-qaid=add-button]').click();
    let jobName = faker.string.alpha(7);
    let description = faker.lorem.sentence();
    let blockSize = faker.number.int(100000).toString();
    // Fill jobNameReference field
    fieldByLabel('Type').click();
    optionList().should('be.visible').contains('Reindex job').click();
    // Fill name and description fields
    inputByLabel('Name').should('be.visible').type(jobName).should('have.value', jobName);
    textAreaByLabel('Description').should('be.visible').type(description).should('have.value', description);
    inputByLabel('Block size').should('be.visible').should('have.value', '1024').clear().type(blockSize).should('have.value', blockSize);
    // Check job appears in list
    cy.get('div[class^=cellContainer]').contains(jobName).should('be.visible');
    // Click Save
    cy.contains('button', 'Save').should('be.visible').click();
    // Check popup notification message
    check.toast('Operation is saved');
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Run job', () => {
    // Prepare data
    const name = faker.string.alpha(7);
    create.job(name);
    goToJobs();
    // Select created job
    cy.intercept('GET', '**/jobs/executions/definition/**').as('jobGet');
    siderList().contains(name).click();
    cy.wait('@jobGet');
    // Click Run
    cy.get('div[data-qaid=startJobButton]').should('be.visible').click();
    cy.get('div[class^=info]').contains('Started').should('be.visible');
    // Check popup notification message
    check.toast('Operation is in progress');
    // Wait for job to finish
    cy.get('div[class^=info]').contains('Completed', { timeout: 60000 }).should('be.visible');
  });

  it('Update job', () => {
    // Prepare data
    const name = faker.string.alpha(7);
    const updName = faker.string.alpha(10).toUpperCase();
    create.job(name);
    goToJobs();
    // Select created job
    siderList().contains(name).click();
    // Fill new values
    inputByLabel('Name').clear().type(updName).should('have.value', updName);
    // Click Save
    cy.contains('button', 'Save').should('be.visible').click();
    // Check popup notification message
    check.toast('Operation is saved');
    // Check display name is updated in job list
    cy.get('div[class^=cellContainer]').contains(updName).should('be.visible');
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Delete job', () => {
    // Prepare data
    const name = faker.string.alpha(7);
    create.job(name);
    goToJobs();
    // Select created job
    siderList().contains(name).click();
    // Click Delete
    headerButton(2).click();
    // Confirmation box should appear
    messageDialog().should('be.visible');
    // Click Continue button
    messageDialog().find('button[data-qaid=confirm]').click();
    // Check popup notification message
    check.toast('Operation is removed');
    // Check job disappear from list
    siderList().should('not.contain.text', name);
    // Check editor is closed
    editor('Jobs').should('not.exist');
  });

  it('Job: Save button state', () => {
    goToJobs();
    // Click Add Job button
    cy.get('button[data-qaid=add-button]').click();
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
    // Fill some property
    inputByLabel('Name').type('something');
    // Check Save button is enabled
    check.buttonIsEnabled(cy.contains('button', 'Save'));
    // Clear the value
    inputByLabel('Name').clear();
    // Check Save button is disabled again
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Job: Required fields', () => {
    goToJobs();
    // Click Add Job button
    cy.get('button[data-qaid=add-button]').click();
    // Fill jobNameReference field only
    fieldByLabel('Type').click();
    optionList().should('be.visible').contains('Reindex job').click();
    // Click Save
    cy.contains('button', 'Save').should('be.visible').click();
    // Check popup notification message
    check.toast('Validation error');
  });

});
