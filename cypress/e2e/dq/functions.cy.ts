import * as table from "../../units/table";
import { goToDataQuality } from "../../actions/goTo";
import { login } from "../../actions/login";

describe('DQ simple: functions', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToDataQuality();
  });

  it('Testing', () => {
    // Switch to Assignments
    cy.get('div[data-component-id=TabBar]').contains('Functions').click();
    // Open Multiply function
    table.treeRowByText('Multiply').scrollIntoView().click();
    // Fill field values
    cy.get('div[class^=portsContainer]').eq(0).find('div[class^=portInput]').eq(0).find('input').type('2').should('have.value', '2');
    cy.get('div[class^=portsContainer]').eq(0).find('div[class^=portInput]').eq(1).find('input').type('2').should('have.value', '2');
    // Press test
    cy.contains('button', 'Test function').click();
    // Check result
    cy.get('div[class^=portsContainer]').eq(1).find('div[class^=portInput]').eq(0).find('div[class^=preview]').should('have.text', '4');
  });

});