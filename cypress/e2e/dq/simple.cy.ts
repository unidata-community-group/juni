import * as check from "../../actions/check";
import * as table from "../../units/table";
import { dropDown, textAreaByLabel, inputByLabel, draftSwitcher, fieldByLabel, optionList, panel, siderListTree } from "../../units/components";
import { goToDataQuality } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import DataQualityPage from '../../po/DataQualityPage';

describe('DQ simple', () => {

  const funcName: string = 'CheckRange';

  beforeEach(() => {
    cy.visit('/');
    login();
    goToDataQuality();
  });

  it('Create DQ rule in simple mode', () => {
    // Prepare data for filling
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const descRule = faker.lorem.sentence();
    const descSet = faker.lorem.sentence();
    const message = faker.lorem.sentence(3);
    const category: string = 'Data is error-free and exact';
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Select entity
    siderListTree().contains('All Attr Types Entity').scrollIntoView().click();
    // Fill rule fields and save
    cy.get('div[data-component-id=CardPanel]').should('be.visible').within(() => {
      // Select attribute
      cy.contains('div[class^=column]', 'intAttr').should('be.visible').click();
      panel('Quality rules').should('be.visible').within(() => {
        // Add rule
        cy.contains('button', 'Create rule').click();
        dropDown().should('be.visible').contains('Validation rule').click();
      });
    });
    // Fill rule fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      // Select function
      fieldByLabel('Function').click();
      dropDown().should('be.visible').contains(new RegExp("^" + funcName + "$", "g")).scrollIntoView().click();
      fieldByLabel('Function').find('div[class^=displayValue]').should('have.text', funcName);
      cy.contains('button', 'Next step').click();
      // Rule properties
      inputByLabel('Name').type(name).should('have.value', name);
      inputByLabel('Display name').type(displayName).should('have.value', displayName);
      textAreaByLabel('Description').type(descRule).should('have.value', descRule);
      fieldByLabel('Run condition').click();
      optionList().should('be.visible').contains('Never').click();
      fieldByLabel('Run condition').find('div[class^=displayValue]').should('have.text', 'Never');
      fieldByLabel('For which source systems to apply').click();
      optionList().should('be.visible').contains('unidata').click();
      fieldByLabel('For which source systems to apply').within(() => {
        cy.get('div[class^=inputValue]').click();
        cy.get('div[class^=option]>span').should('have.text', 'unidata');
      });
      cy.contains('button', 'Next step').click();
      // Validation
      fieldByLabel('Create from').find('div[class^=displayValue]').should('have.text', 'Execution result');
      fieldByLabel('Severity').find('div[class^=displayValue]').should('have.text', 'RED');
      // Set value for message
      fieldByLabel('Message').should('be.visible').find('div[class*=firstItem]').click();
      optionList().should('be.visible').contains('Constant').click();
      textAreaByLabel('Message').should('be.visible').type(message).should('have.text', message);
      // Set value for category
      fieldByLabel('Category').should('be.visible').find('div[class*=firstItem]').click();
      optionList().should('be.visible').contains('Constant').click();
      fieldByLabel('Category').should('be.visible').find('div[class*=lastItem]').click();
      optionList().should('be.visible').contains(category).click();
      fieldByLabel('Category').should('be.visible').find('div[class*=lastItem]').should('have.text', category);
      cy.contains('button', 'Next step').click();
      // Rule set properties
      inputByLabel('Name').should('have.value', 'set_for_allAttrTypesEntity');
      inputByLabel('Display name').should('have.value', 'Set for entity "All Attr Types Entity"');
      textAreaByLabel('Description').type(descSet).should('have.value', descSet);
      fieldByLabel('Phases').click();
      optionList().should('be.visible').contains('Standard phase').click();
      fieldByLabel('Phases').within(() => {
        cy.get('div[class^=inputValue]').click();
        cy.get('div[class^=option]>span').should('have.text', 'Standard phase');
      });
      cy.get('div[class^=portsContainer]').eq(0).should('be.visible').within(() => {
        cy.get('div[class^=portInput]').eq(0).find('textarea').should('have.text', 'register:allAttrTypesEntity:{}.intAttr');
        cy.get('div[class^=portInput]').eq(1).find('input[value=constant]').click();
        cy.get('div[class^=portInput]').eq(1).find('div.ud-input input').should('be.visible').type('3').should('have.value', '3');
        cy.get('div[class^=portInput]').eq(2).find('input[value=constant]').click();
        cy.get('div[class^=portInput]').eq(2).find('div.ud-input input').should('be.visible').type('9').should('have.value', '9');
      });
      cy.get('div[class^=portsContainer]').eq(1).invoke('attr', 'class').should('contain', 'disabled');
      cy.contains('button', 'Submit').click();
    });
    check.toast('Rule is saved');
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new DataQualityPage().publish();
    // Check created rule
    siderListTree().contains('All Attr Types Entity').scrollIntoView().click();
    cy.contains('div[class^=attributeContainer]', 'intAttr').find('div[class^=counter]').should('be.visible').and('have.text', '1');
    table.rowByText(displayName).should('be.visible').within(() => {
      cy.get('div[class^=cellContainer]').eq(1).should('have.text', descRule);
      cy.get('div[class^=cellContainer]').eq(2).should('have.text', 'Never');
      cy.get('div[class^=cellContainer]').eq(3).should('have.text', 'Validate');
      cy.get('div[class^=cellContainer]').eq(4).should('have.text', funcName);
    });
  });

});