import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import { textAreaByLabel, inputByLabel, draftSwitcher, fieldByLabel, optionList, dropDown } from "../../units/components";
import { goToDataQuality } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import DataQualityPage from '../../po/DataQualityPage';

describe('DQ advanced: sets of rules', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToDataQuality();
    // Switch to advanced mode
    cy.get('header button[class*=buttonHasLeftIcon]').should('be.visible').click();
    dropDown().should('be.visible').contains('Switch to advanced mode').click();
  });

  it('Create set of DQ rules', () => {
    // Prepare data
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const description = faker.lorem.sentence();
    const ruleDN = faker.string.alpha({ length: 10, casing: 'upper' });
    const context = 'register:dqEntity:{}.productName';
    create.dqRule(ruleDN);
    // Switch to Set of rules
    cy.get('div[data-component-id=TabBar]').contains('Set of rules').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Create set
    cy.contains('button', 'Create set').click();
    // Fill set fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      inputByLabel('Name').type(name).should('have.value', name);
      inputByLabel('Display name').type(displayName).should('have.value', displayName);
      textAreaByLabel('Description').type(description).should('have.value', description);
      fieldByLabel('Rule').click();
      optionList().should('be.visible').contains(ruleDN).click();
      fieldByLabel('Rule').find('div[class^=displayValue]').should('have.text', ruleDN);
      textAreaByLabel('Context').type(context).should('have.value', context);
      cy.get('div[class^=portsContainer]').eq(1).find('div[class^=portInput]').eq(0).find('textarea').type(context).should('have.value', context);
      cy.contains('button', 'Save').click();
      check.toast('Set is saved');
    });
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new DataQualityPage().publish();
    // Check created rule
    table.rowByText(name).should('be.visible').within(() => {
      cy.get('div[class^=cellContainer]').eq(1).should('have.text', displayName);
      cy.get('div[class^=cellContainer]').eq(2).should('have.text', description);
      cy.get('div[class^=cellContainer]').eq(3).should('have.text', ruleDN);
    });
  });

  it('Edit set of DQ rules', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const updDisplayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const ruleName = faker.string.alpha(10);
    create.dqRule(faker.string.alpha({ length: 10, casing: 'upper' }), ruleName);
    create.dqSet(displayName, ruleName);
    // Switch to Set of rules
    cy.get('div[data-component-id=TabBar]').contains('Set of rules').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Open created set
    table.rowByText(displayName).click();
    // Fill set fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      inputByLabel('Display name').clear().type(updDisplayName).should('have.value', updDisplayName);
      cy.contains('button', 'Save').click();
      check.toast('Set is saved');
    });
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new DataQualityPage().publish();
    // Check updated rule
    table.rowByText(updDisplayName).should('be.visible');
    table.rowByText(displayName).should('not.exist');
  });

  it('Delete set of DQ rules', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const ruleName = faker.string.alpha(10);
    create.dqRule(faker.string.alpha({ length: 10, casing: 'upper' }), ruleName);
    create.dqSet(displayName, ruleName);
    // Switch to Set of rules
    cy.get('div[data-component-id=TabBar]').contains('Set of rules').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Select created set
    table.rowByText(displayName).trigger('mouseover').find('label.ant-checkbox-wrapper').click();
    // Click remove
    cy.contains('button', 'Remove').should('be.visible').click();
    // Click Continue button in confirmation dialog
    cy.get('div[data-qaid=confirmbox]>div').should('be.visible').find('button[data-qaid=confirm]').click();
    cy.get('div[data-qaid=confirmbox]').should('not.exist');
    // Check popup notification message
    check.toast('Set is deleted');
    // Publish draft
    new DataQualityPage().publish();
    // Check set
    table.rowByText(displayName).should('not.exist');
  });

  it('DQ rule set: Required fields', () => {
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Switch to Set of rules
    cy.get('div[data-component-id=TabBar]').contains('Set of rules').click();
    // Create set
    cy.contains('button', 'Create set').click();
    // Fill rule fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      textAreaByLabel('Description').type('something');
      cy.contains('button', 'Save').click();
      check.toast('Validation error');
      check.fieldErrorText('Name', 'Name is required');
      check.fieldErrorText('Display name', 'Display name is required');
      check.fieldErrorText('Rule', 'Rule is required');
    });
  });

});