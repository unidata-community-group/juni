import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import * as open from "../../actions/open";
import { fieldByLabel, panel, draftSwitcher, messageDialog, optionList, fieldValue, modalWindow } from "../../units/components";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import RecordPage from '../../po/RecordPage';

describe('Data records: many2many relations', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
  });

  it('Add many2many relation', () => {
    // Create 'right end' record for relation
    const model = faker.vehicle.model();
    create.automobile(model);
    // Create new record using REST API
    create.person();
    // Prepare data for relation
    const manufactureYear = faker.number.int({ min: 2000, max: 2030 }).toString();
    const mileAge = faker.number.int().toString();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Go to Relations tab
    cy.get('div[data-component-id=TabBar]').contains('Relations').click();
    cy.waitForNetworkIdle(1000);
    // Click Add button
    panel('Cars').should('be.visible').contains('button', 'Add relation').should('be.visible').click();
    // Fill relation attributes
    modalWindow('New relation').should('be.visible').within(() => {
      fieldByLabel('Related record').click();
      optionList().should('be.visible').contains(model).click();
      fieldByLabel('Related record').find('div[class^=displayValue]').should('contains.text', model);
      fieldValue('Manufacture Year').click();
      fieldValue('Manufacture Year').find('input').type(manufactureYear).should('have.value', manufactureYear);
      fieldValue('Mileage').click();
      fieldValue('Mileage').find('input').type(mileAge).should('have.value', mileAge);
      cy.contains('button', 'Create').click();
    });
    modalWindow('New relation').should('not.exist');
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    // Check created relation
    panel('Cars').should('be.visible').within(() => {
      cy.contains('button', 'Add relation').should('not.exist');
      check.cellText(1, 'Record', model);
      check.cellText(1, 'Manufacture Year', manufactureYear);
      check.cellText(1, 'Mileage', mileAge);
    });
  });

  it('Edit many2many relation', () => {
    // Create 'right end' record for relation
    create.automobile();
    // Create new record using REST API
    cy.get('@etalonId').then(relRecordId => {
      create.personWithCar(relRecordId);
    });
    // Prepare data for updated relation
    const updMileAge = faker.number.int().toString();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Go to Relations tab
    cy.get('div[data-component-id=TabBar]').contains('Relations').click();
    cy.waitForNetworkIdle(1000);
    // Open edit dialog
    panel('Cars').should('be.visible').within(() => {
      table.row(1).click();
    });
    // Edit relation attribute
    modalWindow('Edit relation').should('be.visible').within(() => {
      fieldValue('Mileage').click();
      fieldValue('Mileage').find('input').clear().type(updMileAge).should('have.value', updMileAge);
      cy.contains('button', 'Confirm').click();
    });
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    // Check updated relation
    panel('Cars').should('be.visible').within(() => {
      cy.contains('button', 'Add relation').should('not.exist');
      check.cellText(1, 'Mileage', updMileAge);
    });
  });

  it('Delete many2many relation', () => {
    // Create 'right end' record for relation
    create.automobile();
    // Create new record using REST API
    cy.get('@etalonId').then(relRecordId => {
      create.personWithCar(relRecordId);
    });
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Go to Relations tab
    cy.get('div[data-component-id=TabBar]').contains('Relations').click();
    cy.waitForNetworkIdle(1000);
    // Mark relation for delete and click Remove button
    panel('Cars').should('be.visible').within(() => {
      table.row(1).trigger('mouseover');
      table.cell(1, 1).find('span.ant-checkbox').click();
      cy.contains('button', 'Remove relations').should('be.visible').click();
    });
    // Confirm delete
    messageDialog().should('be.visible').find('button[data-qaid=confirm]').click();
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    // Check relation is deleted
    panel('Cars').should('be.visible').within(() => {
      cy.contains('button', 'Add relation').should('not.exist');
      table.row(1).should('not.exist');
    });
  });

});