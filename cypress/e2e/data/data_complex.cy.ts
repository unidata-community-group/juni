import * as check from "../../actions/check";
import * as create from "../../actions/create";
import * as open from "../../actions/open";
import { draftSwitcher, messageDialog, optionList, panel, fieldValue, modalWindow } from "../../units/components";
import { login } from "../../actions/login";
import * as table from "../../units/table";
import { faker } from '@faker-js/faker';
import * as joda from '@js-joda/core';
import * as format from "../../actions/format";
import RecordPage from '../../po/RecordPage';

describe('Data records: complex attributes', () => {

  const docCode = faker.number.int();
  const docName = faker.string.alpha(10);

  before(() => {
    cy.fixture('data/documentType').then((docType) => {
      docType.dataRecord.externalId.externalId = faker.string.uuid();
      docType.dataRecord.codeAttributes[0].value = docCode;
      docType.dataRecord.simpleAttributes[0].value = docName;
      cy.createRecord(docType);
    });
  });

  beforeEach(() => {
    cy.visit('/');
    login();
  });

  it('Add complex attribute', () => {
    // Create new record using REST API
    create.person();
    // Prepare data for complex attribute
    const docNumber = faker.number.int().toString();
    const docIssueDate = format.date(joda.LocalDate.now());
    const docStatus = 'Active';
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Click Add
    panel('Documents').contains('button', 'Add attribute').should('be.visible').click();
    // Fill complex attribute nested record
    modalWindow('Creating a complex attribute record').should('be.visible').within(() => {
      fieldValue('Type').click();
      optionList().should('be.visible').contains(docName).click();
      fieldValue('Status').click();
      optionList().should('be.visible').contains(docStatus).click();
      fieldValue('Number').click();
      fieldValue('Number').find('input').type(docNumber);
      fieldValue('Issue Date').click();
      fieldValue('Issue Date').find('input').type(docIssueDate).type('{enter}');
      cy.contains('button', 'Create').click();
    });
    modalWindow('Creating a complex attribute record').should('not.exist');
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    panel('Documents').contains('button', 'Add attribute').should('not.exist');
    // Check created nested record
    cy.get('div[class^=nestedRecordPanel]').should('be.visible').within(() => {
      fieldValue('Type').find('div[class^=preview]').should('have.text', docName);
      fieldValue('Status').find('div[class^=preview]').should('have.text', docStatus);
      fieldValue('Number').find('div[class^=preview]').should('have.text', docNumber);
      fieldValue('Issue Date').find('div[class^=preview]').should('have.text', docIssueDate);
    });
  });

  it('Edit complex attribute', () => {
    // Create new record using REST API
    cy.fixture('data/personWithDoc').then((personWithDoc) => {
      personWithDoc.dataRecord.externalId.externalId = faker.string.uuid();
      personWithDoc.dataRecord.simpleAttributes[0].value = faker.person.lastName();
      personWithDoc.dataRecord.simpleAttributes[1].value = faker.person.firstName();
      personWithDoc.dataRecord.complexAttributes[0].nestedRecords[0].simpleAttributes[0].value = faker.number.int();
      cy.createRecord(personWithDoc);
    });
    // Prepare data for updated complex attribute
    const updDocNumber = faker.number.int().toString();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Edit number field
    cy.get('div[class^=nestedRecordPanel]').should('be.visible').within(() => {
      fieldValue('Number').should('be.visible').click();
      fieldValue('Number').find('input').clear().type(updDocNumber);
    });
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    panel('Documents').contains('button', 'Add attribute').should('not.exist');
    // Check value
    cy.get('div[class^=nestedRecordPanel]').should('be.visible').within(() => {
      fieldValue('Number').find('div[class^=preview]').should('have.text', updDocNumber);
    });
  });

  it('Delete complex attribute', () => {
    // Create new record using REST API
    const docNumber = faker.number.int();
    cy.fixture('data/personWithDoc').then((personWithDoc) => {
      personWithDoc.dataRecord.externalId.externalId = faker.string.uuid();
      personWithDoc.dataRecord.simpleAttributes[0].value = faker.person.lastName();
      personWithDoc.dataRecord.simpleAttributes[1].value = faker.person.firstName();
      personWithDoc.dataRecord.complexAttributes[0].nestedRecords[0].simpleAttributes[0].value = docNumber;
      cy.createRecord(personWithDoc);
    });
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Switch to table view
    panel('Documents').find('button[data-qaid=changeView]').click();
    // Delete complex attribute
    table.row(1).trigger('mouseover');
    cy.get('button[data-qaid=removeAttribute]').should('be.visible').click();
    messageDialog().find('button[data-qaid=confirm]').click();
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    panel('Documents').contains('button', 'Add attribute').should('not.exist');
    // Check there is no any complex attribute records
    panel('Documents').find('div[class^=infoText]').should('be.visible').and('have.text', 'No data');
  });

});
