import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import * as open from "../../actions/open";
import { panel, draftSwitcher, messageDialog, optionList, fieldValue, modalWindow } from "../../units/components";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import RecordPage from '../../po/RecordPage';

describe('Data records: contains relations', () => {

  const countryName = faker.location.country();
  const countryCode = faker.location.countryCode();

  before(() => {
    cy.fixture('data/country').then((country) => {
      country.dataRecord.externalId.externalId = faker.string.uuid();
      country.dataRecord.codeAttributes[0].value = countryCode;
      country.dataRecord.simpleAttributes[0].value = countryName;
      cy.createRecord(country);
    });
  });

  beforeEach(() => {
    cy.visit('/');
    login();
  });

  it('Add contains relation', () => {
    // Create new record using REST API
    cy.fixture('data/person').then((person) => {
      person.dataRecord.externalId.externalId = faker.string.uuid();
      person.dataRecord.simpleAttributes[0].value = faker.person.lastName();
      person.dataRecord.simpleAttributes[1].value = faker.person.firstName();
      cy.createRecord(person);
    });
    // Prepare data for relation
    const postalCode = faker.location.zipCode();
    const building = faker.number.int().toString();
    const street = faker.location.street();
    const city = faker.location.city();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Go to Relations tab
    cy.get('div[data-component-id=TabBar]').contains('Relations').click();
    cy.waitForNetworkIdle(1000);
    // Click Add button
    panel('Addresses').should('be.visible').contains('button', 'Add relation').should('be.visible').click();
    // Fill relation attributes
    modalWindow('New relation').should('be.visible').within(() => {
      fieldValue('Postal Code').click();
      fieldValue('Postal Code').find('input').type(postalCode).should('have.value', postalCode);
      fieldValue('Building').click();
      fieldValue('Building').find('input').type(building).should('have.value', building);
      fieldValue('Street').click();
      fieldValue('Street').find('input').type(street).should('have.value', street);
      fieldValue('City').click();
      fieldValue('City').find('input').type(city).should('have.value', city);
      fieldValue('Country').click();
      optionList().should('be.visible').contains(countryCode).click();
      fieldValue('Country').find('div[class^=preview]').should('have.text', `${countryCode}, ${countryName}`);
      cy.contains('button', 'Create').click();
    });
    modalWindow('New relation').should('not.exist');
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    // Check created relation
    panel('Addresses').should('be.visible').within(() => {
      cy.contains('button', 'Add relation').should('not.exist');
      check.cellText(1, 'Postal Code', postalCode);
      check.cellText(1, 'Building', building);
      check.cellText(1, 'Street', street);
      check.cellText(1, 'City', city);
      check.cellText(1, 'Country', `${countryCode}, ${countryName}`);
    });
  });

  it('Edit contains relation', () => {
    // Create new record using REST API
    create.personWithAddress();
    // Prepare data for updated relation
    const updPostalCode = faker.location.zipCode();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Go to Relations tab
    cy.get('div[data-component-id=TabBar]').contains('Relations').click();
    cy.waitForNetworkIdle(1000);
    // Open edit dialog
    panel('Addresses').should('be.visible').within(() => {
      table.row(1).click();
    });
    // Edit relation attribute
    modalWindow('Edit relation').should('be.visible').within(() => {
      fieldValue('Postal Code').click();
      fieldValue('Postal Code').find('input').clear().type(updPostalCode).should('have.value', updPostalCode);
      cy.contains('button', 'Confirm').click();
    });
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    // Check updated relation
    panel('Addresses').should('be.visible').within(() => {
      cy.contains('button', 'Add relation').should('not.exist');
      check.cellText(1, 'Postal Code', updPostalCode);
    });
  });

  it('Delete contains relation', () => {
    // Create new record using REST API
    create.personWithAddress();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Go to Relations tab
    cy.get('div[data-component-id=TabBar]').contains('Relations').click();
    cy.waitForNetworkIdle(1000);
    // Mark relation for delete and click Remove button
    panel('Addresses').should('be.visible').within(() => {
      table.row(1).trigger('mouseover');
      table.cell(1, 1).find('span.ant-checkbox').click();
      cy.contains('button', 'Remove relations').should('be.visible').click();
    });
    // Confirm delete
    messageDialog().should('be.visible').find('button[data-qaid=confirm]').click();
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    // Check relation is deleted
    panel('Addresses').should('be.visible').within(() => {
      cy.contains('button', 'Add relation').should('not.exist');
      table.row(1).should('not.exist');
    });
  });

});