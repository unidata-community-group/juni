import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import * as open from "../../actions/open";
import { fieldByLabel, panel, draftSwitcher, messageDialog, optionList, fieldValue, modalWindow } from "../../units/components";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import RecordPage from '../../po/RecordPage';

describe('Data records: reference relations', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
  });

  it('Add reference relation', () => {
    // Create 'right end' record for relation
    const companyName = faker.company.name();
    create.organization(companyName);
    // Create new record using REST API
    create.person();
    // Prepare data for relation
    const positon = faker.person.jobTitle();
    const salary = faker.number.int().toString();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Go to Relations tab
    cy.get('div[data-component-id=TabBar]').contains('Relations').click();
    cy.waitForNetworkIdle(1000);
    // Click Add button
    panel('Place of Work').should('be.visible').contains('button', 'Add relation').should('be.visible').click();
    // Fill relation attributes
    modalWindow('New relation').should('be.visible').within(() => {
      fieldByLabel('Related record').click();
      optionList().should('be.visible').contains(companyName).click();
      fieldByLabel('Related record').find('div[class^=displayValue]').should('contains.text', companyName);
      fieldValue('Salary').click();
      fieldValue('Salary').find('input').type(salary).should('have.value', salary);
      fieldValue('Position').click();
      fieldValue('Position').find('input').type(positon).should('have.value', positon);
      cy.contains('button', 'Create').click();
    });
    modalWindow('New relation').should('not.exist');
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    // Check created relation
    panel('Place of Work').should('be.visible').within(() => {
      cy.get('div[class^=relationLink] a').eq(0).should('have.text', companyName);
      cy.get('div[class^=containerTimeline_]').eq(0).trigger('pointerover');
    });
    cy.get('div[class*=popoverPlacementBottom]').should('be.visible').and('contains.text', salary).and('contains.text', positon);
  });

  it('Edit reference relation', () => {
    // Create 'right end' record for relation
    create.organization();
    // Create new record using REST API
    cy.get('@etalonId').then(relRecordId => {
      create.personWithPlaceOfWork(relRecordId);
    });
    // Prepare data for updated relation
    const updSalary = faker.number.int().toString();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Go to Relations tab
    cy.get('div[data-component-id=TabBar]').contains('Relations').click();
    cy.waitForNetworkIdle(1000);
    // Open edit dialog
    panel('Place of Work').should('be.visible').within(() => {
      cy.get('div[class^=containerTimeline_]').eq(0).trigger('pointerover');
    });
    cy.get('div[class*=popoverPlacementBottom]').should('be.visible').find('button').eq(0).click();
    // Edit relation attribute
    modalWindow('Edit relation').should('be.visible').within(() => {
      fieldValue('Salary').click();
      fieldValue('Salary').find('input').clear().type(updSalary).should('have.value', updSalary);
      cy.contains('button', 'Confirm').click();
    });
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    // Check updated relation
    panel('Place of Work').should('be.visible').within(() => {
      cy.get('div[class^=containerTimeline_]').eq(0).trigger('pointerover');
    });
    cy.get('div[class*=popoverPlacementBottom]').should('be.visible').and('contains.text', updSalary);
  });

  it('Delete reference relation', () => {
    // Create 'right end' record for relation
    create.organization();
    // Create new record using REST API
    cy.get('@etalonId').then(relRecordId => {
      create.personWithPlaceOfWork(relRecordId);
    });
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Go to Relations tab
    cy.get('div[data-component-id=TabBar]').contains('Relations').click();
    cy.waitForNetworkIdle(1000);
    // Mark relation for delete and click Remove button
    panel('Place of Work').should('be.visible').within(() => {
      cy.get('div[class^=deleteButton]>button').eq(0).should('be.visible').click();
    });
    // Confirm delete
    messageDialog().should('be.visible').find('button[data-qaid=confirm]').click();
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    // Check relation is deleted
    panel('Place of Work').should('be.visible').within(() => {
      cy.get('div[class^=addDeleteButtons]>button').should('not.exist');
      table.row(1).should('not.exist');
    });
  });

});