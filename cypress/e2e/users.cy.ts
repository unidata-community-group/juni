import * as check from "../actions/check";
import { siderList, inputByName, inputByLabel, actionsButton } from "../units/components";
import { goToUsers } from "../actions/goTo";
import { login } from "../actions/login";
import { faker } from '@faker-js/faker';

describe('Users', () => {

  const updFN = faker.person.firstName();
  const updLN = faker.person.lastName();
  const updUsername = (updFN.charAt(0).toLowerCase() + updLN.toLowerCase()).replace("'", '');

  before(() => {
    cy.createUser(updFN, updLN, updUsername);
  });

  beforeEach(() => {
    cy.visit('/');
    login();
    goToUsers();
  });

  it('Create user', () => {
    // Click Add User button
    actionsButton(1).click();
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    const username = (firstName.charAt(0).toLowerCase() + lastName.toLowerCase()).replace("'", '');
    // Fill user fields
    inputByLabel('Name').should('be.visible').type(firstName).should('have.value', firstName);
    inputByLabel('Last name').should('be.visible').type(lastName).should('have.value', lastName);
    inputByLabel('Login').should('be.visible').type(username).should('have.value', username);
    inputByLabel('Email').should('be.visible').type(username + '@unidata.com').should('have.value', username + '@unidata.com');
    inputByName('password').should('be.visible').type(username).should('have.value', username);
    inputByName('passwordConfirmation').should('be.visible').type(username).should('have.value', username);
    inputByName('admin').check().should('be.checked');
    // Check user appears in list
    siderList().contains(username).parent().parent().should('contain.text', firstName).and('contain.text', lastName);
    // Click Save
    cy.contains('button', 'Save').should('be.visible').click();
    // Check popup notification message
    check.toast('User is saved');
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Update user', () => {
    // Select created user
    siderList().contains(updUsername).click();
    const newFirstName = faker.person.firstName();
    const newLastName = faker.person.lastName();
    // Fill new values
    inputByLabel('Name').clear().type(newFirstName).should('have.value', newFirstName);
    inputByLabel('Last name').clear().type(newLastName).should('have.value', newLastName);
    // Click Save
    cy.contains('button', 'Save').should('be.visible').click();
    // Check popup notification message
    check.toast('User is saved');
    // Check first name and last name are updated in list
    siderList().contains(updUsername).parent().parent().should('contain.text', newFirstName).and('contain.text', newLastName);
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('User: Save button state', () => {
    // Click Add User button
    actionsButton(1).click();
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
    // Fill some property
    inputByLabel('Name').type('something');
    // Check Save button is enabled
    check.buttonIsEnabled(cy.contains('button', 'Save'));
    // Clear the value
    inputByLabel('Name').clear();
    // Check Save button is disabled again
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('User: Required fields', () => {
    // Click Add User button
    actionsButton(1).click();
    // Fill some property
    inputByName('admin').check();
    // Click Save
    cy.contains('button', 'Save').should('be.visible').click();
    // Check popup notification message
    check.toast('Validation error');
    check.fieldErrorText('Name', 'Required field');
    check.fieldErrorText('Last name', 'Required field');
    check.fieldErrorText('Login', 'Required field');
    check.fieldErrorText('Email', 'Required field');
    check.fieldErrorText('New password', 'Password should be set');
    check.fieldErrorText('Confirm password', 'Password should be set');
  });

});
