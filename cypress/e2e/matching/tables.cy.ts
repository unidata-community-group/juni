import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import { textAreaByLabel, inputByLabel, draftSwitcher, panel } from "../../units/components";
import { goToMatching } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import MatchingPage from '../../po/MatchingPage';

describe('Matching: tables', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToMatching();
  });

  it('Create matching table', () => {
    // Prepare data for filling
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha(10).toUpperCase();
    const description = faker.lorem.sentence();
    const columnName = faker.string.alpha(10);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add table
    cy.contains('button', 'Create table').should('be.visible').click();
    // Fill table fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      panel('General').within(() => {
        inputByLabel('Name').type(name).should('have.value', name);
        inputByLabel('Display name').type(displayName).should('have.value', displayName);
        textAreaByLabel('Description').type(description).should('have.value', description);
      });
      panel('Matching columns').within(() => {
        inputByLabel('Matching object').type(columnName).should('have.value', columnName);
        inputByLabel('Display name').should('have.value', columnName);
      });
      cy.contains('button', 'Save').click();
      check.toast('Table is saved');
    });
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new MatchingPage().publish();
    // Check created table
    table.rowByText(name).should('be.visible').within(() => {
      cy.get('div[class^=cellContainer]').eq(0).should('have.text', displayName);
      cy.get('div[class^=cellContainer]').eq(2).should('have.text', description);
      cy.get('div[class^=cellContainer]').eq(3).should('have.text', columnName);
    });
  });

  it('Update matching table', () => {
    // Prepare data
    const displayName = faker.string.alpha(10).toUpperCase();
    const updDisplayName = faker.string.alpha(10).toUpperCase();
    create.matchingTable(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Open created table
    table.rowByText(displayName).click();
    // Update display name and delete one of matching columns
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      panel('General').within(() => {
        inputByLabel('Display name').clear().type(updDisplayName).should('have.value', updDisplayName);
      });
      cy.get('section[class^=itemContainer]').eq(1).find('button').should('be.visible').click();
      cy.contains('button', 'Save').click();
      check.toast('Table is saved');
    });
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new MatchingPage().publish();
    // Check updated table
    table.rowByText(updDisplayName).should('be.visible').within(() => {
      cy.get('div[class^=cellContainer]').eq(3).should('have.text', 'Column 1');
    });
    table.rowByText(displayName).should('not.exist');
  });

  it('Delete matching table', () => {
    // Prepare data
    const displayName = faker.string.alpha(10).toUpperCase();
    create.matchingTable(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Select created table
    table.rowByText(displayName).trigger('mouseover').find('label.ant-checkbox-wrapper').click();
    // Click delete
    cy.contains('button', 'Delete table').should('be.visible').click();
    // Click Continue button in confirmation dialog
    cy.get('div[data-qaid=confirmbox]>div').should('be.visible').find('button[data-qaid=confirm]').click();
    cy.get('div[data-qaid=confirmbox]').should('not.exist');
    // Check popup notification message
    check.toast('Table is deleted');
    // Publish draft
    new MatchingPage().publish();
    // Check table
    table.rowByText(displayName).should('not.exist');
  });

  it('Matching table: Required fields', () => {
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add table
    cy.contains('button', 'Create table').click();
    // Fill table field and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      inputByLabel('Matching object').type(faker.string.alpha(10));
      cy.contains('button', 'Save').click();
      check.toast('Validation error');
      panel('General').within(() => {
        check.fieldErrorText('Name', 'Name is required');
        check.fieldErrorText('Display name', 'Display name is required');
      });
    });
  });

});