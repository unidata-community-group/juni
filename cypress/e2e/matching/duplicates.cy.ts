import { goToDuplicates } from "../../actions/goTo";
import * as check from "../../actions/check";
import * as create from "../../actions/create";
import * as open from "../../actions/open";
import { fieldValue, dropDown, panel } from "../../units/components";
import { login } from "../../actions/login";
import * as table from "../../units/table";
import { faker } from '@faker-js/faker';
import RecordPage from '../../po/RecordPage';

describe('Matching: duplicates', () => {

  const ruleName1 = 'One Value';
  const ruleName3 = 'Three Values';
  const setName = 'Full Name';
  const newRecordUrl = '/#/dataviewlight/register/person/_new/?draftId=';

  beforeEach(() => {
    cy.visit('/');
    login();
  });

  it('Create cluster', () => {
    // Create one record using REST API
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    create.person(firstName, lastName);
    // Create duplicate
    cy.visit(newRecordUrl);
    check.toast('Draft is saved');
    cy.get('[data-qaid=dataViewPage]').should('be.visible');
    // Fill record attributes
    cy.get('section#dataViewerContent').should('be.visible').within(() => {
      fieldValue('First Name').click();
      fieldValue('First Name').find('input').type(firstName);
      fieldValue('Last Name').click();
      fieldValue('Last Name').find('input').type(lastName);
    });
    // Click Save button
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    // Check cluster
    panel('Clusters').should('be.visible').within(() => {
      cy.get('span[class^=linkContainer]').eq(0).should('be.visible').within(() => {
        cy.get('a').should('be.visible').and('have.attr', 'href').and('contain', 'cluster');
        cy.get('span[class^=titleText]').should('be.visible').and('have.text', `${setName} > ${ruleName3}`);
        cy.get('div[class^=counter]').should('be.visible').and('have.text', '2');
      });
    });
  });

  it('Search cluster', () => {
    // Create records using REST API
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    create.person(firstName, lastName);
    create.person(firstName, lastName);
    // Go to Duplicates
    goToDuplicates();
    // Add rule and set to search criteria
    cy.get('button[data-qaid=addCriteria]').click();
    cy.get('div[data-qaid=criteriaTreeDropDownContainer]').should('be.visible').within(() => {
      cy.get("div[data-qaid='item_$ruleName']").click();
      cy.get("div[data-qaid='item_$setName']").click();
      cy.get("div[data-qaid='item_$ownerNamespace']").click();
    });
    // Fill search criteria for negative result
    cy.get("div[class^=tagsList] div[data-qaid='$setName']").click();
    cy.get("div[data-qaid='$setName_dropdown']").should('be.visible').contains(setName).click();
    cy.get("div[class^=tagsList] div[data-qaid='$ruleName']").click();
    cy.get("div[data-qaid='$ruleName_dropdown']").should('be.visible').contains(ruleName1).click();
    // Click Search
    cy.get('button[data-qaid=searchButton]').should('be.visible').click();
    // Check number of clusters found
    cy.get('div[data-component-id=CardPanel] div[class^=cardTitle]').should('have.text', 'Found: 0');
    // Fill search criteria for positive result
    cy.get("div[class^=tagsList] div[data-qaid='$ruleName']").click();
    cy.get("div[data-qaid='$ruleName_dropdown']").should('be.visible').contains(ruleName3).click();
    cy.get("div[class^=tagsList] div[data-qaid='$ownerNamespace']").click();
    cy.get("div[data-qaid='$ownerNamespace_dropdown']").should('be.visible').contains('Entities').click();
    cy.get('div[class^=tagsList] div[data-qaid=register]').click();
    cy.get('div[data-qaid=register_dropdown]').should('be.visible').contains('Person').click();
    cy.get('div[class^=tagsList] div[data-qaid=records]').click();
    cy.get('div[data-qaid=records_dropdown]').first().should('be.visible').within(() => {
      cy.get('input[data-qaid=querySearch]').should('be.visible').type(lastName);
      cy.get('@etalonId').then(id => {
        cy.get(`div[data-qaid=item_${id}]`).should('be.visible').click();
      });
    });
    // Click Search
    cy.get('button[data-qaid=searchButton]').should('be.visible').click();
    // Check number of clusters found
    cy.get('div[data-component-id=CardPanel] div[class^=cardTitle]').should('have.text', 'Found: 1');
    // Check found cluster
    table.cell(1, 2).should('have.text', `${lastName}, ${firstName}`);
    table.cell(1, 3).should('have.text', ruleName3);
    table.cell(1, 4).should('have.text', setName);
    table.cell(1, 5).should('have.text', '2');
  });

  it('Merge cluster', () => {
    // Create records using REST API
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    const birthPlace1 = faker.location.city();
    const birthPlace2 = faker.location.city();
    cy.fixture('data/person').then((person) => {
      person.dataRecord.externalId.externalId = faker.string.uuid();
      person.dataRecord.simpleAttributes[0].value = lastName;
      person.dataRecord.simpleAttributes[1].value = firstName;
      person.dataRecord.simpleAttributes.push({
        "value": birthPlace1,
        "type": "String",
        "name": "birthPlace"
      });
      cy.createRecord(person);
    });
    cy.fixture('data/person').then((person) => {
      person.dataRecord.externalId.externalId = faker.string.uuid();
      person.dataRecord.simpleAttributes[0].value = lastName;
      person.dataRecord.simpleAttributes[1].value = firstName;
      person.dataRecord.simpleAttributes.push({
        "value": birthPlace2,
        "type": "String",
        "name": "birthPlace"
      });
      cy.createRecord(person);
    });
    // Open created cluster
    cy.get('@etalonId').then(id => {
      cy.getRecordInfo(id);
      cy.get('@clusters').then((clusters) => {
        cy.visit(`/#/cluster/${clusters[0].id}`);
      });
    });
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      cy.get('div[class^=leftChild]').should('contain.text', ruleName3).and('contain.text', setName);
      cy.get('div[data-component-id=CardPanel][class*=internal]').should('be.visible').within(() => {
        cy.get('div[class^=cardTitle]').should('have.text', 'Total: 2');
        cy.get('div[class^=headerText]>label.ant-checkbox-wrapper').click();
        cy.get('button[data-qaid=clusterActions]').should('be.visible').click();
        dropDown().should('be.visible').contains('Compare').click();
      });
    });
    // Merge records
    cy.get('div[data-qaid=recordsCompare]').should('be.visible').within(() => {
      cy.contains('button', 'Merge records').should('be.visible').click();
    });
    // Click Continue button
    cy.get('div[data-qaid=confirmbox]>div').should('be.visible').find('button[data-qaid=confirm]').click();
    cy.get('div[data-qaid=confirmbox]').should('not.exist');
    // Check popup notification message
    check.toast('Records merged');
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Check records are merged into one
    cy.get('@etalonId').then(id => {
      cy.getRecordInfo(id);
      cy.get('@keys').then((keys) => {
        expect(keys.originKeys).to.have.lengthOf(2);
      });
    });
    // Open merged record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });

    // Check attribute values
    cy.get('section#dataViewerContent').should('be.visible').within(() => {
      fieldValue('First Name').find('div[class^=preview]').should('have.text', firstName);
      fieldValue('Last Name').find('div[class^=preview]').should('have.text', lastName);
      fieldValue('Place of Birth').find('div[class^=preview]').should('have.text', birthPlace2);
    });
  });

});

