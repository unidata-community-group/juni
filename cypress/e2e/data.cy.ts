import * as check from "../actions/check";
import * as create from "../actions/create";
import * as open from "../actions/open";
import { draftSwitcher, fieldValue, dropDown, messageDialog } from "../units/components";
import { login } from "../actions/login";
import { faker } from '@faker-js/faker';
import RecordPage from '../po/RecordPage';

describe('Data records', () => {

  const newRecordUrl = '/#/dataviewlight/register/person/_new/?draftId=';

  beforeEach(() => {
    cy.visit('/');
    login();
  });

  it('Create record', () => {
    // Prepare data
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    cy.visit(newRecordUrl);
    check.toast('Draft is saved');
    cy.get('[data-qaid=dataViewPage]').should('be.visible');
    // Check data card
    cy.get('div[class^=groupTitle]').should('have.text', 'Data Processing');
    cy.get('div[class^=primaryTitle]').should('have.text', 'Person');
    cy.get('div[class^=afterTitle]>span[data-component-id=Tag]').should('have.text', 'Draft');
    // Fill record attributes
    cy.get('section#dataViewerContent').should('be.visible').within(() => {
      fieldValue('First Name').click();
      fieldValue('First Name').find('input').type(firstName).should('have.value', firstName);
      fieldValue('Last Name').click();
      fieldValue('Last Name').find('input').type(lastName).should('have.value', lastName);
    });
    // Click Save button
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Record is saved');
    // Check record is updated on UI
    cy.get('span[class^=secondaryTitle]').should('contain.text', firstName).and('contain.text', lastName);
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
    // Publish draft
    new RecordPage().publish();
    // Check data
    cy.get('section#dataViewerContent').should('be.visible').within(() => {
      fieldValue('First Name').find('div[class^=preview]').should('have.text', firstName);
      fieldValue('Last Name').find('div[class^=preview]').should('have.text', lastName);
      // Check enriched read-only field
      fieldValue('Full Name').should('have.attr', 'data-readonly');
      fieldValue('Full Name').find('div[class^=preview]').should('have.text', `${firstName} ${lastName}`.toUpperCase());
    });
  });

  it('Update record', () => {
    // Create new record using REST API
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    create.person(firstName, lastName);
    // Prepare data for update
    const newFirstName = faker.person.firstName();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Check data card
    cy.get('div[class^=groupTitle]').should('have.text', 'Data Processing');
    cy.get('div[class^=primaryTitle]').should('have.text', 'Person');
    cy.get('span[class^=secondaryTitle').should('contain.text', lastName).and('contain.text', firstName);
    cy.get('div[class^=afterTitle]>span').should('not.exist');
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    cy.get('section#dataViewerContent').should('be.visible').within(() => {
      fieldValue('First Name').click();
      fieldValue('First Name').find('input').clear().type(newFirstName).should('have.value', newFirstName);
    });
    // Click Save button
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Record is saved');
    // Check record is updated on UI
    cy.get('span[class^=secondaryTitle').should('contain.text', lastName).and('contain.text', newFirstName);
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
    // Publish draft
    new RecordPage().publish();
    // Check data
    cy.get('section#dataViewerContent').should('be.visible').within(() => {
      fieldValue('First Name').find('div[class^=preview]').should('have.text', newFirstName);
      fieldValue('Last Name').find('div[class^=preview]').should('have.text', lastName);
      // Check enriched read-only field
      fieldValue('Full Name').should('have.attr', 'data-readonly');
      fieldValue('Full Name').find('div[class^=preview]').should('have.text', `${newFirstName} ${lastName}`.toUpperCase());
    });
  });

  it('Restore record', () => {
    // Create new record using REST API
    create.person();
    cy.get('@etalonId').then(id => {
      // Delete that record using REST API
      cy.deleteRecord('person', id);
      // Open deleted record
      open.recordByEtalonId('person', id);
    });
    // Check record marked as Removed
    cy.get('div[class^=afterTitle]>span').should('have.text', 'Removed');
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Check Restore button
    cy.contains('button', 'Restore').should('be.visible');
    check.buttonIsEnabled(cy.contains('button', 'Restore'));
    // Restore record
    let recordPage = new RecordPage();
    recordPage.restore();
    recordPage.publish();
    // Check record is not marked as Removed
    cy.get('div[class^=afterTitle]>span').should('not.exist');
  });

  it('Delete record', () => {
    // Create new record using REST API
    create.person();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Open additional actions menu
    cy.get('header button[class*=buttonHasLeftIcon]').should('be.visible').click();
    // Click Delete
    dropDown().should('be.visible').contains('Delete record').click();
    // Click Continue button
    messageDialog().should('be.visible').find('button[data-qaid=confirm]').click();
    messageDialog().should('not.exist');
    // Check popup notification message
    check.toast('Record is deleted');
    // Check data card is closed
    cy.get('section[data-qaid=dataViewPage]').should('not.exist');
  });

  it('Record: Save button state', () => {
    // Click Create Record button
    cy.visit(newRecordUrl);
    check.toast('Draft is saved');
    cy.get('[data-qaid=dataViewPage]').should('be.visible');
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
    // Fill record attributes
    cy.get('section#dataViewerContent').should('be.visible').within(() => {
      fieldValue('First Name').click();
      fieldValue('First Name').find('input').type('smth');
    });
    // Check Save button is enabled
    check.buttonIsEnabled(cy.contains('button', 'Save'));
    // Clear the value
    cy.get('section#dataViewerContent').should('be.visible').within(() => {
      fieldValue('First Name').click();
      fieldValue('First Name').find('input').clear();
    });
    // Check Save button is disabled again
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Record: Required fields', () => {
    // Click Create Record button
    cy.visit(newRecordUrl);
    check.toast('Draft is saved');
    cy.get('[data-qaid=dataViewPage]').should('be.visible');
    // Fill record attributes
    cy.get('section#dataViewerContent').should('be.visible').within(() => {
      fieldValue('First Name').click();
      fieldValue('First Name').find('input').type('smth');
    });
    // Click Publish button
    cy.contains('button', 'Publish').click();
    // Check popup notification message
    check.toast('Record has validation errors');
  });

});
