
export function columnFilterButton(columnName: string) {
    return cy.xpath(`.//div[text()='${columnName}']/ancestor::div[contains(@class,'headContainer')][1]//button`);
}

export function columnHeader(columnName: string) {
    return cy.xpath(`.//div[text()='${columnName}']/ancestor::div[contains(@class,'headerTextContainer')][1]`);
}

export function columnSortIcon(columnName: string) {
    return cy.xpath(`.//div[text()='${columnName}']/ancestor::div[contains(@class,'headerTextContainer')][1]/span[contains(@class,'sortableIcon')]`);
}

export function row(index: number) {
    return cy.get(`div[class^=rowContainer]:nth-child(${index})`);
}

export function rowByText(text: string) {
    return cy.xpath(`.//*[text()='${text}']/ancestor::div[starts-with(@class,'row')][1]`);
}

export function getColumnIndex(columnName: string) {
    cy.get('div[class^=headerRow]').contains('div', columnName).invoke('index').as('columnIndex');
}

export function cell(rowIndex: number, columnIndex: number) {
    return cy.get(`div[class^=rowContainer]:nth-child(${rowIndex})>div:nth-child(${columnIndex})`);
}

export function treeRow(index: number) {
    return cy.get(`div[class^=treeContainer]>div[class^=row]:nth-child(${index})`);
}

export function treeCell(rowIndex: number, columnIndex: number) {
    return cy.get(`div[class^=treeContainer]>div[class^=row]:nth-child(${rowIndex})>div:nth-child(${columnIndex})`);
}

export function treeRowByText(text: string) {
    return cy.xpath(`.//span[text()='${text}']/ancestor::div[starts-with(@class,'row')][1]`);
}