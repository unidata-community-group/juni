// Buttons

export function headerButton(position: number) {
    return cy.get('header div[class*=sContainer] button').eq(position - 1);
}

export function actionsButton(position: number) {
    return cy.get('div[class^=actionsGroup] button').eq(position - 1);
}

export function buttonByName(name: string) {
    return cy.get(`button[name=${name}]`);
}

export function draftSwitcher() {
    return cy.xpath(`.//span[text()='Draft']/ancestor::span[@data-component-id ='Switcher'][1]/button`);
}

// Fields

export function fieldByLabel(label: string) {
    return cy.xpath(`.//*[text()='${label}']/ancestor::div[@class='ud-field'][1]`);
}

export function fieldById(id: string) {
    return cy.get(`div[data-component-id="Field"][data-qaid=${id}]`);
}

export function inputByLabel(label: string) {
    return fieldByLabel(label).find('div[data-component-id=Input] input');
}

export function textAreaByLabel(label: string) {
    return fieldByLabel(label).find('div[data-component-id=FieldInput] textarea');
}

export function checkboxByLabel(label: string) {
    return fieldByLabel(label).find('label.ant-checkbox-wrapper');
}

export function radioByLabel(name: string) {
    return cy.xpath(`.//span[text()='${name}']/ancestor::label[@class='ant-radio-wrapper'][1]`);
}

export function fieldErrorByLabel(label: string) {
    return fieldByLabel(label).find('div[class^=fieldError]');
}

export function selectByLabel(label: string) {
    return fieldByLabel(label).find('div.ud-select>div');
}

export function inputByName(name: string) {
    return cy.get(`input[name = ${name}]`);
}

export function textAreaByName(name: string) {
    return cy.get(`textarea[name = ${name}]`);
}

export function fieldValue(label: string) {
    return fieldByLabel(label).find('div[data-component-id=Input]');
}

// Modals

export function messageDialog() {
    return cy.document().its('body').find('div[data-qaid=confirmbox]>div');
}

export function modalWindowDialog() {
    return cy.document().its('body').find('div[data-qaid=modal]>div');
}

export function modalWindow(name: string) {
    return cy.xpath(`.//div[text()='${name}']/ancestor::div[starts-with(@class,'modal')][1]`);
}

// Panels, containers

export function siderList() {
    return cy.get('div[class^=siderListContainer] div[class^=udTableCommon]');
}

export function siderListTree() {
    return cy.get('div[class^=siderListContainer] div[class^=treeContainer]');
}

export function editor(name: string) {
    return cy.xpath(`.//div[starts-with(@class,'primaryTitle')]/span[text()='${name}']/ancestor::header[1]`);
}

export function panel(title: string) {
    return cy.xpath(`.//*[starts-with(normalize-space(translate(., '\u00A0\u00A0', ' ')),'${title}') and starts-with(@class, 'cardTitle')]/ancestor::div[@data-component-id='CardPanel'][1]`);
}

// Dropdowns

export function optionList() {
    return cy.document().its('body').find('div[data-component-id=OptionList]');
}

export function dropDown() {
    return cy.document().its('body').find('div[data-component-id=DropDown]');
}

export function tooltip() {
    return cy.document().its('body').find('div[class*=popover]>div[class^=block]:not([class*=hasShadow])');
}

// Misc
export function pageBody() {
    return cy.document().find('body');
}
