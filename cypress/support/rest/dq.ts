Cypress.Commands.add('createRule', (rule) => {
  Cypress.log({
    name: 'createRule',
    message: `${rule}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/data-quality/quality-rules/rules?draftId=0',
    body: rule,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createSetOfRules', (set) => {
  Cypress.log({
    name: 'createSetOfRules',
    message: `${set}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/data-quality/quality-rules/sets?draftId=0',
    body: set,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createAssignments', (setName, entityName) => {
  Cypress.log({
    name: 'createAssignments',
    message: `${setName} | ${entityName}`
  });
  return cy.request({
    method: 'PUT',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/data-quality/quality-rules/assignments/register?draftId=0',
    body: {
      'assignment':
      {
        "nameSpace": "register",
        "assignments": [
          {
            "entityName": entityName,
            "assignment": [
              {
                "phase": "",
                "sets": [
                  setName
                ]
              }
            ]
          },
          {
            "entityName": "person",
            "assignment": [
              {
                "phase": "",
                "sets": [
                  "set_for_person"
                ]
              }
            ]
          },
          {
            "entityName": "product",
            "assignment": [
              {
                "phase": "",
                "sets": [
                  "set_for_product"
                ]
              }
            ]
          }
        ]
      }
    },
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('importDQModel', (filePath) => {
  Cypress.log({
    name: 'importDQModel',
    message: `${filePath}`
  });
  const formData = new FormData();
  cy.fixture(filePath, 'binary').then((file) => {
    formData.set('file', Cypress.Blob.binaryStringToBlob(file, 'text/xml'), 'dq.xml');
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/data-quality/model/import',
    body: formData,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.status).to.eq(200);
  });
});