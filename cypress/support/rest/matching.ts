Cypress.Commands.add('createMatchingTable', (table) => {
  Cypress.log({
    name: 'createMatchingTable',
    message: `${table}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/matching/matching-tables/?draftId=0',
    body: table,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createMatchingRule', (rule) => {
  Cypress.log({
    name: 'createMatchingRule',
    message: `${rule}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/matching/rules/?draftId=0',
    body: rule,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createMatchingSet', (set) => {
  Cypress.log({
    name: 'createMatchingSet',
    message: `${set}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/matching/sets/?draftId=0',
    body: set,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createMatchingAssignment', (assignment) => {
  Cypress.log({
    name: 'createMatchingAssignment',
    message: `${assignment}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/matching/assignments/?draftId=0',
    body: assignment,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('importMatchingModel', (filePath) => {
  Cypress.log({
    name: 'importMatchingModel',
    message: `${filePath}`
  });
  const formData = new FormData();
  cy.fixture(filePath, 'binary').then((file) => {
    formData.set('file', Cypress.Blob.binaryStringToBlob(file, 'text/xml'), 'matching.xml');
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/matching/model/import',
    body: formData,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.status).to.eq(200);
  });
});