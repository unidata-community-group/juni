Cypress.Commands.add('createDraft', (draft) => {
  Cypress.log({
    name: 'createDraft',
    message: `${draft}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/draft/upsert',
    body: draft,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
    cy.wrap(response.body.draft.draftId).as('draftId');
  });
});

Cypress.Commands.add('publishDraft', (draftId) => {
  Cypress.log({
    name: 'publishDraft',
    message: `${draftId}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/draft/publish',
    body: {
      'draftId': draftId,
      'force': false,
      'delete': true
    },
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('removeAllDrafts', () => {
  Cypress.log({
    name: 'removeAllDrafts'
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/draft/remove',
    body: {},
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});
