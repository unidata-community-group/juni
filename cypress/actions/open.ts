/**
 * Opens record by given entity name and etalon id.
 * @example recordByEtalonId('person', 'etalon-id')
 */
export function recordByEtalonId(entityName: string, etalonId: string) {
    cy.visit(`/#/dataviewlight/register/${entityName}/${etalonId}`);
    cy.get('[data-qaid=dataViewPage]').should('be.visible');
    cy.waitForNetworkIdle(1000);
}
