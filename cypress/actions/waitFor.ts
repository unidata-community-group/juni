import { recurse } from 'cypress-recurse';

export function notification() {
    recurse(
        () => {
            return cy.request({
                method: 'GET',
                url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/core/user/notifications/count',
                headers: {
                    'Authorization': Cypress.env('token')
                }
            });
        },
        (response) => response.body.count > 0,
        {
            timeout: Cypress.config('requestTimeout'),
            delay: 500,
            limit: 100,
            log: false,
            error: 'Notification count wasn\'t changed for ' + Cypress.config('requestTimeout') + ' ms'
        }
    );
}
