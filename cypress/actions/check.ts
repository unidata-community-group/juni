import * as table from "../units/table";
import { fieldErrorByLabel, tooltip } from "../units/components";

/**
 * Checks notification popup (toast) text. Closes all visible popups after that.
 * @example toast('This is awesome text')
 */
export function toast(text: string) {
    cy.document().its('body').find('div[data-qaid=toast]').should('be.visible').and('contains.text', text).
        find('div[class^=closeButton]>svg').click({ multiple: true });
    cy.document().its('body').find('div[data-qaid=toast]').should('not.exist');
}

/**
 * Checks button is disabled.
 * @example buttonIsDisabled(buttonToCheck)
 */
export function buttonIsDisabled(button: any) {
    button.invoke('attr', 'class').should('contain', 'buttonIsDisabled');
}

/**
 * Checks button is enabled.
 * @example buttonIsEnabled(buttonToCheck)
 */
export function buttonIsEnabled(button: any) {
    button.invoke('attr', 'class').should('not.contain', 'buttonIsDisabled');
}

/**
 * Checks tab is disabled.
 * @example tabIsDisabled(tabToCheck)
 */
export function tabIsDisabled(tab: any) {
    tab.invoke('attr', 'class').should('contain', 'isDisabled');
}

/**
 * Checks records count in Search UI.
 * @example recordCountInSearch(expectedCount)
 */
export function recordsCountInSearchIs(count: number) {
    cy.waitForNetworkIdle(1000);
    cy.get('div[class*=udTableCommon] div[class^=rowContainer]').should('have.length', count);
    cy.contains('Found records').should('have.text', `Found records: ${count}`);
}

/**
 * Checks cell text found by row index and column name
 * @example cellText(1, 'Column 1', 'expected text')
 */
export function cellText(rowIndex: number, columnName: string, expectedText: string) {
    cy.contains('div[class^=headerRow]>div', columnName).invoke('index').then((columnIndex) => {
        table.cell(rowIndex, columnIndex + 1).should('have.text', expectedText);
    });
}

/**
 * Checks field error text
 * @example fieldErrorText('Name', 'expected text')
 */
export function fieldErrorText(fieldName: string, expectedText: string) {
    fieldErrorByLabel(fieldName).trigger('pointerover');
    tooltip().should('be.visible').and('have.text', expectedText);
    fieldErrorByLabel(fieldName).trigger('pointerout');
}
