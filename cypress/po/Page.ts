export default abstract class Page {

    checkToast(msg: string) {
        cy.document().its('body').find('div[data-qaid=toast]').should('be.visible').and('contains.text', msg).
            find('div[class^=closeButton]>svg').click({ multiple: true });
        cy.document().its('body').find('div[data-qaid=toast]').should('not.exist');
    }

}